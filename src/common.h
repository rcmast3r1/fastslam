
#pragma once

#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include <vector>
#include "Eigen/Dense"
//

using Eigen::MatrixXd;
//dynamic-size matrix of doubles

using Eigen::VectorXd;
//

//https://eigen.tuxfamly.org/dox/classEigen_1_1Matrix.html

using std::vector;

#define debug 0

using namespace std;
